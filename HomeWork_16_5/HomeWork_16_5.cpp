#include <iostream>
#include <string>
#include <ctime>
#pragma warning(disable : 4996)

using namespace std;

int main()
{
    const int N = 4;

    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j];
            
        }
        cout << '\n';
    }
    cout << '\n';

    int sum = 0;
    time_t t;
    time(&t);
    int day = localtime(&t)->tm_mday; 

    for (int i = 0; i < N; i++)
    {
        sum += array[day % N][i];
    }
    cout << sum << endl;

    return 0;
          
}

