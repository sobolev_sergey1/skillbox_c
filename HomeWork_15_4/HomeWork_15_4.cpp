#include <iostream>
#include <cmath>
using namespace std;

void Numer(int ParityNumberFinder, bool odd)
{
    for (int i = odd; i <= ParityNumberFinder; i += 2)
    {
        cout << i << " ";
    }
    
    cout << "\n";
}

int main()
{
    int ParityNumberFinder;

    cout << "Enter a number from 0 to N \n" << "Parity Number Finder = "; 
    cin >> ParityNumberFinder;

    cout << "Odd: ";
    Numer(ParityNumberFinder, 1);
    

    cout << "Even: ";
    Numer(ParityNumberFinder, 0);
    

    return 0;
}